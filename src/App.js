import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { selectList, getProductList, selectLoader } from './store/list';

import Favorites from './pages/Favorites';
import Basket from './pages/Basket';
import Home from './pages/Home';
import './style/style.scss';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";

const mapStateToProps = state => ({
  list: selectList(state),
  isDataLoaded: selectLoader(state),
});

const App = connect(mapStateToProps, { getProductList })(({ list, getProductList, isDataLoaded }) => {

  useEffect(() => {
    getProductList()
  }, [])

  return (
    <Router>
      <div className={'app'}>

        <header className="header">
          <NavLink to="/" exact>Home</NavLink>
          <NavLink to="/fav">Favorites</NavLink>
          <NavLink to="/basket">Basket</NavLink>
        </header>

        <Switch>
          <Route path="/fav">
            <Favorites list={list} />
          </Route>

          <Route path="/basket">
            <Basket list={list} />
          </Route>

          <Route path="/" exact>
            <Home list={list} load={isDataLoaded}/>
          </Route>

        </Switch>
      </div>
    </Router>
  );
});


export default App;