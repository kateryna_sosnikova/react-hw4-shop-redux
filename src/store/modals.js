// Action types
const OPEN_MODAL = "OPEN_MODAL";
const OPEN_MODAL_DELETE = "OPEN_MODAL_DELETE";

// SELECTORS
export const MODULE_NAME = 'modals';
export const selectModal = state => state[MODULE_NAME].modal;
export const selectModalDelete = state => state[MODULE_NAME].modalDelete;
export const selectId = state => state[MODULE_NAME].id;

// Reducer
const initialState = {
    modal: false,
    modalDelete: false, 
    id: null
}

export function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case OPEN_MODAL:
            return {
                ...state,
                modal: !state.modal,
                id: payload
            }
        case OPEN_MODAL_DELETE:
            return {
                ...state,
                modalDelete: !state.modalDelete,
                id: payload
            }
        // Default
        default:
            return state
    }
};

// Action creators
export const openModal = payload => ({
    type: OPEN_MODAL,
    payload
});
export const openModalDelete = payload => ({
    type: OPEN_MODAL_DELETE,
    payload
});


export const getOpenModal = (id) => dispatch => {
    dispatch(openModal(id));
}

export const getOpenDeleteModal = (id) => dispatch => {
    dispatch(openModalDelete(id));
}



