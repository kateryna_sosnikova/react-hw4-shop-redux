import {
    createStore,
    combineReducers,
    applyMiddleware
} from 'redux';

import thunk from 'redux-thunk';
import { composeWithDevTools} from 'redux-devtools-extension';

import { reducer as modalReducer, MODULE_NAME as modalsModuleName } from './modals';
import { reducer as listReducer, MODULE_NAME as listModuleName } from './list';

const rootReducer = combineReducers({
    [modalsModuleName]: modalReducer,
    [listModuleName]: listReducer
})

// Create our store
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;




