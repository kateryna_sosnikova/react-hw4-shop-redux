// Action types
const SET_LIST = "SET_LIST";
const ADD_FAVORITE = "ADD_FAVORITE";
const REMOVE_FAVORITE = "REMOVE_FAVORITE";
const ADD_BASKET = "ADD_BASKET";
const REMOVE_BASKET = "REMOVE_BASKET";
const SET_LOADER = "SET_LOADER";

// SELECTORS
export const MODULE_NAME = 'list';
export const selectList = state => state[MODULE_NAME].list;
export const selectLoader = state => state[MODULE_NAME].isDataLoaded;

// Reducer
const initialState = {
    list: [],
    isDataLoaded: false
}

export function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case SET_LOADER:
            return {
                ...state, 
                isDataLoaded: true
            }
        case SET_LIST:
            return {
                ...state,
                list: payload,
                isDataLoaded: false
            }
        case ADD_FAVORITE:
            return {
                ...state,
                list: payload
            }
        case REMOVE_FAVORITE:
            return {
                ...state,
                list: payload
            }
        case ADD_BASKET:
            return {
                ...state,
                list: payload
            }
        case REMOVE_BASKET:
            return {
                ...state,
                list: payload
            }
        // Default
        default:
            return state
    }
};

// Action creators
export const setList = payload => ({
    type: SET_LIST,
    payload
});

export const setAddFavorite = payload => ({
    type: ADD_FAVORITE,
    payload
});

export const setRemoveFavorite = payload => ({
    type: REMOVE_FAVORITE,
    payload
});

export const setAddBasket = payload => ({
    type: ADD_FAVORITE,
    payload
});

export const setRemoveBasket = payload => ({
    type: ADD_FAVORITE,
    payload
});

export const setLoader = () => ({
    type: SET_LOADER
})

// MIDDLEWARE

export const getProductList = () => async (dispatch) => {

    const arrayFav = localStorage.getItem('Favorites') ? JSON.parse(localStorage.getItem('Favorites')) : []
    const arrayBasket = localStorage.getItem('Basket') ? JSON.parse(localStorage.getItem('Basket')) : []

    if (arrayFav || arrayBasket) {
        dispatch(setLoader());
        fetch('./product.json')
            .then(res => res.json())
            .then(data => {
                dispatch(setList(data.map(item => {
                    if (arrayFav) {
                        arrayFav.forEach(card => {
                            if (item.id === card.id) {
                                item.inFavorite = true;
                            }
                        })
                    }
                    if (arrayBasket) {
                        arrayBasket.forEach(card => {
                            if (item.id === card.id) {
                                item.inBasket = true;
                            }
                        })
                    }
                    return item;
                })))
            })
    } else {
        dispatch(setLoader());
        fetch('./product.json')
            .then(res => res.json())
            .then(data => dispatch(setList(data)))
    }
}
