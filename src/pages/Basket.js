import React from 'react';
import { Card } from '../components/Card';

const Basket = ({ list }) => {

    return (
        <>
            <ul className='cards-container'>
                {
                    list.map(item => {
                        if (item.inBasket) {
                            return (
                                <Card
                                    {...item}
                                    key={item.id}
                                    card={item}
                                    page='true'
                                />
                            )
                        };
                    }
                    )}
            </ul>
        </>
    )

}

export default Basket;