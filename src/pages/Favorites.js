import React from 'react';
import { Card } from '../components/Card'

const Favorites = ({list}) => {
    return (
                    <>
                        <ul className='cards-container'>
                            {
                                list.map(item => {
                                    if (item.inFavorite) {
                                        return (<Card
                                            {...item}
                                            key={item.id}
                                            card={item}
                                        />)
                                    }
                                }
                                )}
                        </ul>
                    </>
    )
}

export default Favorites;



