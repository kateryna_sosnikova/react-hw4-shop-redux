import React from 'react';
import { Card } from '../components/Card';
//import { selectLoader } from '../store/list';
import { Loader } from '../components/Loader'

const Home = ({list, load}) => {
    return (
        <>
            {load ? <Loader /> :
            <ul className='cards-container'>
                {
                    list.map(item =>
                        <Card
                            {...item}
                            key={item.id}
                            card={item}
                        />
                    )}
            </ul>
}
        </>
    )
}

export default Home;