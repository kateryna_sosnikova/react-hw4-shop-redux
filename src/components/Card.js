import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button'
import Modal from './Modal'
import { FaRegStar } from "react-icons/fa";
import { FaStar } from "react-icons/fa";
import { FaTrash } from "react-icons/fa";

import { connect } from 'react-redux';
import { selectModal, selectModalDelete, selectId, getOpenModal, getOpenDeleteModal } from '../store/modals';
import { selectList, setAddFavorite, setRemoveFavorite, setAddBasket, setRemoveBasket} from '../store/list'

const mapStateToProps = state => ({
    stateModal: selectModal(state),
    stateModaleDelete: selectModalDelete(state),
    idRedux: selectId(state),
    list: selectList(state)
});

export const Card = connect(mapStateToProps, {getOpenModal, getOpenDeleteModal, setAddFavorite, setAddBasket, 
    setRemoveFavorite, setRemoveBasket})((props) => {

    const { list, name, price, article, url, color, inFavorite, inBasket, 
        setAddFavorite, setRemoveFavorite, id, idRedux, card, page, stateModal, 
        stateModaleDelete, getOpenModal, getOpenDeleteModal } = props;

      // console.log('idRedux', idRedux);

    const addToFavorite = (card) => {
        const favoriteList = list.map(item => {
            if (item.id === card.id) {
                item.inFavorite = true;
            } 
            return item;
        })
        setAddFavorite(favoriteList);
        localStorage.setItem('Favorites', JSON.stringify(favoriteList.filter(item => item.inFavorite)));
    }

    const removeFromFavorite = (card) => {
        const favoriteList = list.map(item => {
            if (item.id === card.id) {
                item.inFavorite = false;
            } 
            return item;
        })
        setRemoveFavorite(favoriteList);
        localStorage.setItem('Favorites', JSON.stringify(favoriteList.filter(item => item.inFavorite)));
    }

    const addToBasket = (card) => {
        const basketList = list.map(item => {
            if (item.id === card.id) {
                item.inBasket = true;
            }
            return item;
        })
        setAddBasket(basketList);
        localStorage.setItem('Basket', JSON.stringify(basketList.filter(item => item.inBasket)));
    }

    const removeFromBasket = (card) => {
        const basketList = list.map(item => {
            if (item.id === card.id) {
                item.inBasket = false;
            }
            return item;
        })
        setRemoveBasket(basketList);
        localStorage.setItem('Basket', JSON.stringify(basketList.filter(item => item.inBasket)));
    }

    const btnContent = {
        text: "Add To Card",
        onClick: () => getOpenModal(id)
    }

    const modalContent = {
        text: "Вы уверены, что хотите добавить товар в корзину?",
        header: "Товар будет добавлен в корзину",
        onClick: () => getOpenModal(id),
        actions: [<Button text="ОК" onClick={() => { addToBasket(card); getOpenModal(id) }} />,
        <Button text="Нет, не нужно" onClick={() => getOpenModal(id)} />]
    }

    const modalRemoveContent = {
        text: "Вы уверены, что хотите удалить товар?",
        header: "Товар будет безвозвратно удален и вы можете забыть о нем",
        onClick: () => getOpenDeleteModal(id),
        actions: [<Button text="ОК" onClick={() => { removeFromBasket(card); getOpenDeleteModal(id) }} />,
        <Button text="Нет, хочу оставить" onClick={() => getOpenDeleteModal(id)} />]
    }


    return (
        <li className="card-item" id={id}>
            <img className="card-image" src={url} alt='item-pic' />
            { !inFavorite &&
                <span className="favorite-icon" onClick={() => { addToFavorite(card); }}><FaRegStar /></span>}
            { inFavorite && <span className="favorite-icon" onClick={() => { removeFromFavorite(card); }}><FaStar /></span>}
            { inBasket && page && <span className="trash-icon" onClick={() => {getOpenDeleteModal(id)}}><FaTrash /></span>}
            { stateModaleDelete && id === idRedux &&
                <Modal {...modalRemoveContent} />}
            <h2 className="card-name">{name}</h2>
            <p className="card-article">Article: {article}</p>
            <p className="card-color">Color: {color}</p>
            <p className="card-price">Price: {price}</p>
            {!page && <Button {...btnContent} />}
            { id === idRedux && stateModal &&
                <Modal {...modalContent} />}
        </li>

    )
})

//export default Card;

Card.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    article: PropTypes.string,
    url: PropTypes.string,
    color: PropTypes.string,
    id: PropTypes.number
}

Card.defaultProps = {
    price: '$9.99'
}