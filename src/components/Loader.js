import React from 'react';

export const Loader = () => {

    return (
        <div className="loader-container">
            <div className="preloader"></div>
            <p className='loader-text'>Loading</p>
        </div>
    );
}

